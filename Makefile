PROGRAM=txdconv
SRC=./src/*.c
OUTPUT=./bin/txdconv

CFLAGS=-Wall -g

MKDIR_BIN=mkdir-bin

default: $(MKDIR_BIN)
	$(CC) $(CFLAGS) $(SRC) -o $(OUTPUT)

$(MKDIR_BIN):
	mkdir -p bin

clean:
	rm -rf ./bin