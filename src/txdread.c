#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

#include "include/txdread.h"
#include "include/utils.h"

TxdReader_t *TxdReader_t_Create(char *filepath)
{
  TxdReader_t *reader = malloc(sizeof(TxdReader_t));
  if ((reader->file = fopen(filepath, "rb")) == NULL)
  {
    printf("Cannot open %s\n", filepath);
    return NULL;
  }
  else
  {
    return reader;
  }
}

void TxdReader_t_Destroy(TxdReader_t *reader)
{
  printf("Destroy reader\n");
  assert(reader && reader->file);
  fclose(reader->file);
  free(reader);
}

void TxdReader_t_Read(TxdReader_t *reader)
{
  assert(reader && reader->file);
  HeaderInfo_t header = TxdReader_t_ReadHeader(reader);

  if (header.id != CHUNK_TEXDICTIONARY)
  {
    printf("Not valid txd file\n");
    return;
  }

  header = TxdReader_t_ReadHeader(reader);
  reader->count = readUInt16(reader->file); // number of textures
  fseek(reader->file, 2, SEEK_CUR);         // skip unknown data

  for (uint32_t i = 0; i < reader->count; i++)
  {
    printf("Read #%d TEXTURE\n", i);
    TxdReader_t_ReadHeader(reader); // Texture Native
    TxdReader_t_ReadHeader(reader); // Texture Native chunk data
    uint32_t platform = readUInt32(reader->file);
    uint32_t filterFlags = readUInt32(reader->file);
    printf("PLATFORM: %u\n", platform);
    printf("FILTER FLAGS: %u\n", filterFlags);
    char textureName[32];
    fread(textureName, sizeof(char), 32, reader->file);
    printf("TEXTURE NAME: %s\n", textureName);
    char alphaName[32];
    fread(alphaName, sizeof(char), 32, reader->file);
    printf("ALPHA NAME: %s\n", alphaName);
    uint32_t alphaFlags = readUInt32(reader->file);
    printf("ALPHA FLAGS: %u\n", alphaFlags);
    uint32_t d3dFormat = readUInt32(reader->file);
    printf("D3D FORMAT: %u\n", d3dFormat);
    uint16_t width = readUInt16(reader->file);
    printf("WIDTH: %u\n", width);
    uint16_t height = readUInt16(reader->file);
    printf("HEIGHT: %u\n", height);
    uint8_t depth = readUInt8(reader->file);
    printf("DEPTH: %u\n", depth);
    uint8_t mipmapCount = readUInt8(reader->file);
    printf("MIPMAP COUNT: %u\n", mipmapCount);
    uint8_t texcode = readUInt8(reader->file);
    printf("TEXCODE: %u\n", texcode);
    uint8_t flags = readUInt8(reader->file);
    printf("FLAGS: %u\n", flags);
    uint16_t paletteSize = depth == 8 ? 256 * 4 : 0;
    uint8_t palette[paletteSize];
    printf("Palette size: %u\n", paletteSize);

    if (paletteSize)
    {
      fread(palette, sizeof(uint8_t), paletteSize, reader->file);
    }

    for (uint32_t j = 0; j < mipmapCount; j++)
    {
      uint32_t textureSize = readUInt32(reader->file);
      printf("Texture size: %u\n", textureSize);
      uint8_t data[textureSize];
      fread(data, sizeof(uint8_t), textureSize, reader->file);

      FILE *outputFile = fopen("output.tga", "wb");

      writeUInt8(0, outputFile);
      writeUInt8(0, outputFile);
      writeUInt8(2, outputFile);
      writeUInt16(0, outputFile);
      writeUInt16(0, outputFile);
      writeUInt8(0, outputFile);
      writeUInt16(0, outputFile);
      writeUInt16(0, outputFile);
      writeUInt16(width, outputFile);
      writeUInt16(height, outputFile);
      writeUInt8(0x20, outputFile);
      writeUInt8(0x28, outputFile);

      for (uint32_t k = 0; k < width * height; k++)
      {
        writeUInt8(data[k * 4 + 0], outputFile); // Red
        writeUInt8(data[k * 4 + 1], outputFile); // Green
        writeUInt8(data[k * 4 + 2], outputFile); // Blue
        writeUInt8(data[k * 4 + 3], outputFile); // Alpha
      }

      fclose(outputFile);
    }
  }
}

HeaderInfo_t TxdReader_t_ReadHeader(TxdReader_t *reader)
{
  assert(reader && reader->file);
  HeaderInfo_t header;
  uint32_t buffer[HEADER_BUFFER_SIZE];
  fread(buffer, sizeof(uint32_t), HEADER_BUFFER_SIZE, reader->file);
  header.id = buffer[0];
  header.size = buffer[1];
  header.marker = buffer[2];
  HeaderInfo_t_Dump(header);
  return header;
}

void HeaderInfo_t_Dump(HeaderInfo_t header)
{
  char *headerName = HeaderInfo_t_GetName(header);
  printf(
      "*** HEADER DUMP ***\n"
      "* Header: %s (%u)\n"
      "* Size: %u\n"
      "* Marker: %u\n",
      headerName,
      header.id,
      header.size,
      header.marker);
}

char *HeaderInfo_t_GetName(HeaderInfo_t header)
{
  switch (header.id)
  {
  case CHUNK_DATA:
    return "Chunk Data";
  case CHUNK_TEXNATIVE:
    return "Texture Native";
  case CHUNK_TEXDICTIONARY:
    return "Texture Dictionary";
  default:
    return "UNKNOWN_HEADER";
  }
}
