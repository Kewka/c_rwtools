#include "include/utils.h"

uint32_t readUInt32(FILE *file)
{
    uint32_t value;
    fread(&value, sizeof(uint32_t), 1, file);
    return value;
}

uint16_t readUInt16(FILE *file)
{
    uint16_t value;
    fread(&value, sizeof(uint16_t), 1, file);
    return value;
}

uint8_t readUInt8(FILE *file)
{
    uint8_t value;
    fread(&value, sizeof(uint8_t), 1, file);
    return value;
}

void writeUInt16(uint16_t value, FILE *file)
{
    fwrite(&value, sizeof(uint16_t), 1, file);
}
void writeUInt32(uint32_t value, FILE *file)
{
    fwrite(&value, sizeof(uint32_t), 1, file);
}
void writeUInt8(uint8_t value, FILE *file)
{
    fwrite(&value, sizeof(uint8_t), 1, file);
}