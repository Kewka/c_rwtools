#include <stdio.h>
#include <stdint.h>

uint16_t readUInt16(FILE *file);
uint32_t readUInt32(FILE *file);
uint8_t readUInt8(FILE *file);
void writeUInt16(uint16_t value, FILE *file);
void writeUInt32(uint32_t value, FILE *file);
void writeUInt8(uint8_t value, FILE *file);