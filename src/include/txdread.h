#include <stdio.h>
#include <stdint.h>

#define HEADER_BUFFER_SIZE 3

// Structs
typedef struct TxdReader
{
    FILE *file;
    uint16_t count;
} TxdReader_t;

typedef struct HeaderInfo
{
    uint32_t id;
    uint32_t size;
    uint32_t marker;
} HeaderInfo_t;

// Enums

typedef enum ChunkType
{
    CHUNK_DATA = 0x01,
    CHUNK_TEXNATIVE = 0x15,
    CHUNK_TEXDICTIONARY = 0x16
} ChunkType;

// Functions
TxdReader_t *TxdReader_t_Create(char *filepath);
void TxdReader_t_Destroy(TxdReader_t *reader);
void TxdReader_t_Read(TxdReader_t *reader);
HeaderInfo_t TxdReader_t_ReadHeader(TxdReader_t *reader);

void HeaderInfo_t_Dump(HeaderInfo_t header);
char *HeaderInfo_t_GetName(HeaderInfo_t header);