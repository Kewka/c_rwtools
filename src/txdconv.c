#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "include/txdread.h"

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Use: txdconv [file]\n");
        return -1;
    }

    char *filepath = argv[1];
    printf("File: %s\n", filepath);

    TxdReader_t *reader = TxdReader_t_Create(filepath);

    if (!reader)
    {
        return 1;
    }

    TxdReader_t_Read(reader);
    TxdReader_t_Destroy(reader);

    return 0;
}
